window.openWeather.factory('Xhr', function ($http) {
  return {
    // Define XHR get service
    get: function(url, callback) {
      $http({
        method: 'GET',
        url: url
      }).then(function(res) {
        callback(null, res.data);
      }, callback)
    }
  };
});