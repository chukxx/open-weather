window.openWeather.component('cityCard', {
  templateUrl: './components/city-card/index.html',
  bindings: {
    city: '='
  }
});