window.CityController = function ($scope, $mdDialog, city) {
  $scope.city = city;
  $scope.formatDate = function (date) {
    return window.moment(+date * 1000).format('LT');
  };

  var plotGraph = function () {
    var maxScale = 0;
    var tempData = city.data.list.map(function (day) {
      var temp = Number(day.main.temp.toFixed(2));
      maxScale = maxScale < temp ? temp : maxScale;
      return temp;
    });

    var windData = city.data.list.map(function (day) {
      return Number(day.wind.speed.toFixed(2));
    });

    var labels = city.data.list.map(function (day) {
      return moment(+day.dt * 1000).fromNow();
    });

    var myLineChart = new window.Chart('chart', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Temperature °C',
          borderColor: 'rgba(255,64,129,1)',
          backgroundColor: 'rgba(255,255,255,0)',
          fill: false,
          data: tempData,
        }, {
          label: 'Wind Speed (km/h)',
          borderColor: 'rgba(50,50,50,1)',
          backgroundColor: 'rgba(255,255,255,0)',
          data: windData
        }],
        fill: false,
        labels: labels,
      },
      options: {
        fill: false,
        responsive: true,
        legend: {
          display: true,
          usePointStyle: true,
          labels: {
            fontColor: 'rgb(255, 99, 132)'
          }
        },
        hover: {
          mode: 'index'
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              suggestedMax: maxScale * 1.1
            }
          }]
        }
      }
    });
  };

  $scope.init = function () {
    setTimeout(plotGraph, 200);
  };
};