window.openWeather.component('materialIcon', {
  templateUrl: './components/material-icon/index.html',
  bindings: {
    name: '=',
    isLoading: '='
  }
});