(function () {
  'use strict';
  window.openWeather = angular.module('openWeather', [
    'ngMaterial'
  ]);

  window.openWeather.controller('AppCtrl', ['Xhr', '$mdDialog', '$mdToast',
    function (Xhr, $mdDialog, $mdToast) {
      var $scope = this;
      var init = function () {
        $scope.count = 5;
        $scope.isLoading = false;
        // List of cities with OpenWeather IDs
        $scope.cities = [{
          photo: 'nairobi.jpg',
          name: 'Nairobi',
          id: 184745
        }, {
          photo: 'london.jpg',
          name: 'London',
          id: 2643743
        }, {
          photo: 'paris.jpg',
          name: 'Paris',
          id: 2988507
        }, {
          photo: 'lagos.jpg',
          name: 'Lagos',
          id: 2332459
        }, {
          photo: 'berlin.jpg',
          name: 'Berlin',
          id: 2950159
        }, {
          photo: 'amsterdam.jpg',
          name: 'Amsterdam',
          id: 2759794
        }];

        $scope.getData();
      };

      // Load and populate cities array with OpenWeatherData
      $scope.getData = function () {
        if ($scope.isLoading) {
          return;
        }

        var count = 0;
        $scope.isLoading = true;
        // Clean each city data
        $scope.cities = $scope.cities.map(function (city) {
          delete city.data;
          return city;
        });

        // Load each city data into pertinent object
        $scope.cities.forEach(function (city, idx) {
          Xhr.get('http://api.openweathermap.org/data/2.5/forecast?id=' +
            city.id + '&appid=3d8b309701a13f65b660fa2c64cdc517&cnt=' +
            $scope.count + '&units=metric',
            function (err, res) {
              if (err) {
                showToast('Error fetching data for ' + city.name);
              } else {
                city.data = res;
                $scope.cities[idx] = city;
              }

              count++;
              // Remove spinner after loading last city data
              if (count === $scope.cities.length) {
                $scope.isLoading = false;
              }
            });
        });
      };

      var showToast = function (text) {
        $mdToast.show(
          $mdToast.simple()
            .textContent(text)
            .position('bottom right')
            .hideDelay(3000)
        );
      };

      $scope.showCity = function (ev, idx) {
        var city = $scope.cities[idx];
        if (city.data) {
          $mdDialog.show({
            locals: { city: city },
            controller: window.CityController,
            templateUrl: './components/city/index.html',
            parent: angular.element(document.body),
            targetEvent: ev,
            clickOutsideToClose: true,
            fullscreen: true
          });
        } else {
          showToast('City data not loaded');
        }
      };

      // Init app
      init();
    }]);
})();