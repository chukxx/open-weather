# Open Weather

### Dependencies
- Angular _1.5.11_

- Angular Material

- Moment.js

- Chart.js

- OpenWeather Fonts

### How it works

- The app requires a basic http server to run, because I used a couple of components (as reusabled code was recommended in the exercise) and they won't be fetched with file:/// protocol. You can simply run `npm start` in the app's directory (preinstall scripts got you covered ;).

- The main controller contains a list of cities and their Open Weather IDs.

- A XHR is made to pull and populate the defined city data with the current forecast data.

- Cards are used to display the Cities with a custom photo loaded from the `/images/` directory.

- Clicking on a Card brings up a dialgo showing the forecasets for the next couple of hours as well as a Graph showing Temperature and Wind Speed changes.

- The application uses the __metric__ unit system which is akin to myself and Backbase, so we good on that lol.

- The component for the button icons is in the `app/components/material-icon/` directory.

- The component for the city _modal/dialog view_ is in the `app/components/city/` directory.

- The component for the city _card view_ is in the `app/components/city-card/` directory.

### NB
- The application's UX/UI obeys Google Material design specs AF.


Haben spaß.